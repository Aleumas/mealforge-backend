class IngredientsController < ApplicationController
    def index
        ingredients = Ingredient.all
        render json: ingredients
    end

    def show
        render json: Ingredient.find(params[:id])
    end

    def search
        input = params["input"]
        if input
            sanitizedClause = ActiveRecord::Base.sanitize_sql_for_conditions(["name ILIKE ?", "#{input}%"])
            ingredients = Ingredient.where(sanitizedClause).limit(5)
            render json: ingredients
        else
            render json: Ingredient.order("RANDOM()").limit(5)
        end
    end

end