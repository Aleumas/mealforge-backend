class RecipesController < ApplicationController

    def index

        @recipes = Recipe.all.map do |recipe|

            {
                recipe: recipe,
                tags: recipe.tags,
                steps: recipe.steps,
                ingredients: recipe.ingredients
            }

        end

        render json: @recipes

    end

    def create

        @recipe = Recipe.find_or_initialize_by(recipe_params)
        
        tags = tag_params[:value]
        steps = step_params[:value]
        ingredients = ingredient_params[:value]

        unless @recipe.persisted? 
            
            tags.each do |value| 
                @recipe.tags.build(value: value).save
            end

            steps.each_with_index do |instruction, index| 
                @recipe.steps.build(instruction: instruction, index: index + 1).save
            end

            ingredients.each do |id| 
                ingredient = Ingredient.find_by(id: id)
                @recipe.ingredients << ingredient
            end

            @recipe.save

        end

        render plain: "'#{@recipe.name}' was successfully created!" 

    end

    private

    def recipe_params
        params.require(:recipe).permit(:name, :category, :user_id)
    end

    def tag_params
        params.require(:tags).permit(value: [])
    end

    def step_params
        params.require(:steps).permit(value: [])
    end

    def ingredient_params
        params.require(:ingredients).permit(value: [])
    end

end