class CreateRecipes < ActiveRecord::Migration[7.1]
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :category
      t.text :user_id

      t.timestamps
    end
  end
end
