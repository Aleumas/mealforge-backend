class CreateSteps < ActiveRecord::Migration[7.1]
  def change
    create_table :steps do |t|
      t.text :instruction
      t.integer :index

      t.timestamps
    end
  end
end
