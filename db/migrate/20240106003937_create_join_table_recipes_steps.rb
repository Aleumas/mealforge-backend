class CreateJoinTableRecipesSteps < ActiveRecord::Migration[7.1]
  def change
    create_join_table :recipes, :steps do |t|
      # t.index [:recipe_id, :step_id]
      # t.index [:step_id, :recipe_id]
    end
  end
end
