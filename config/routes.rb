Rails.application.routes.draw do
  get "up" => "rails/health#show", as: :rails_health_check

  get "/ingredients/search(/:input)", to: "ingredients#search"

  resources :recipes, only: [:index, :create]
  resources :ingredients, only: [:index, :show]

end
